FROM docker.mirror.hashicorp.services/alpine:latest
LABEL maintainer="Yurii Hryhortsevych <yuriy.grigortsevich@smartexe.com>"

ENV PACKER_VERSION=1.8.2
ENV PACKER_SHA256SUM=1c8c176dd30f3b9ec3b418f8cb37822261ccebdaf0b01d9b8abf60213d1205cb

RUN apk add --update \
    ansible \
    bash \
    git \
    openssh \
    openssl \
    wget \ 
 && rm -rf /var/cache/apk/* 

ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip ./
ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_SHA256SUMS ./

RUN sed -i '/.*linux_amd64.zip/!d' packer_${PACKER_VERSION}_SHA256SUMS
RUN sha256sum -cs packer_${PACKER_VERSION}_SHA256SUMS
RUN unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /bin \
 && rm -f packer_${PACKER_VERSION}_linux_amd64.zip

ENTRYPOINT []